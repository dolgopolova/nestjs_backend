import { EntityRepository, Repository } from "typeorm";
import { BookReceiveModel } from "../models/books/book.receive.model";
import { BookModel } from "../models";
import { BookEntity } from "../entities/book.entity";
import { ApplicationException } from "../common/exceptions/application.exception";

@EntityRepository(BookEntity)
export class BookRepository extends Repository<BookEntity> {
    
    public async createBook(model: BookReceiveModel, dateTime: string): Promise<void>{
        const book = await this.create({
            creationDate: dateTime,
            name: model.name,
            authors: model.authors,
            description: model.description,
            price: model.price,
            year: model.year,
            category: model.category
        });
        await this.insert(book);
    }

    public async getBookById(id: string): Promise<BookEntity> {
        const book = await this.findOne(id);
        if (!book) {
            throw new ApplicationException(400, 'Book not found');
        }
        return book;
    }

    public async updateBook(model: BookModel): Promise<void> {
        const book = await this.findOne(model.id);
        if (!book) {
            throw new ApplicationException(400, 'Сannot update an book');
        }
        await this.update(model.id, model);
    }

    public async deleteBookById(id: string): Promise<void> {
        const book = await this.findOne(id);
        if (!book) {
            throw new ApplicationException(400, 'Сannot delete an book');
        }
        await this.delete(id);
    }

    public async getAll(): Promise<BookEntity[]> {
        const books = await this.find();
        books.sort(function(a, b) {
            const firstDate = new Date(a.creationDate);
            const secondDate = new Date(b.creationDate);
            return firstDate > secondDate ? -1 : firstDate < secondDate ? 1 : 0;
            });
        return books;
    }
 }