import { EntityRepository, Repository } from "typeorm";
import { PurchaseItemEntity } from "../entities/purchaseItem.entity";
import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { ApplicationException } from "../common/exceptions/application.exception";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";

@EntityRepository(PurchaseItemEntity)
export class PurchaseRepository extends Repository<PurchaseItemEntity> {

    public async createPurchase(purchase: NewPurchaseItemModel, dateTime: string): Promise<boolean> {
        const cart = await this.findPurchaseByUserIdProductId(purchase.userId, purchase.productId);
        if (cart)
        {
            cart.qty += 1;
            await this.updatePurchaseByUserIdProductId(new PurchaseItemModel(cart));
            return true;
        }

        const newPurchase = await this.create({
            creationDate: dateTime,
            name: purchase.name,
            userId: purchase.userId,
            productId: purchase.productId,
            qty: purchase.qty,
            type: purchase.type,
            costsPerOne: purchase.costsPerOne
        });
        await this.insert(newPurchase);
        return true;
    }
    
    public async updatePurchaseByUserIdProductId(usersPurchase: PurchaseItemModel): Promise<boolean> {
        const purchase = await this.findOne({ userId: usersPurchase.userId, productId: usersPurchase.productId});
        if (!purchase) {
            throw new ApplicationException(400, 'Purchase was not found');
        }
        await this.update(usersPurchase.id, usersPurchase);
        return true;
    }

    public async getPurchaseByUserId(id: string): Promise<PurchaseItemEntity[]> {
        const purchase = await this.find({ userId: id});
        return purchase;
    }

    public async findPurchaseByUserIdProductId(userId: string, productId: string): Promise<PurchaseItemEntity> {
        const purchase = await this.findOne({userId: userId, productId: productId});
        return purchase;
    }

    public async deletePurchaseByUserIdProductId(purchaseId: string): Promise<boolean> {
        await this.delete(purchaseId);
        return true;
    }
}