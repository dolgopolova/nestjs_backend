import {EntityRepository, Repository} from "typeorm";
import {MagazineEntity} from "../entities";
import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
import { MagazineModel } from "../models";
import { ApplicationException } from "../common/exceptions/application.exception";

@EntityRepository(MagazineEntity)
export class MagazineRepository extends Repository<MagazineEntity> {

    public async createMagazine(model: MagazineReceiveModel, dateTime: string): Promise<void>{
        const magazine = this.create({
            creationDate: dateTime,
            name: model.name,
            publisher: model.publisher,
            price: model.price,
            year: model.year,
            category: model.category
        });
        await this.insert(magazine);
    }

    public async getMagazineById(id: string): Promise<MagazineEntity> {
        const magazine = await this.findOne(id);
        if (!magazine) {
            throw new ApplicationException(400, 'Magazine not found');
        }
        return magazine;
    }

    public async updateMagazine(model: MagazineModel): Promise<void> {
        const magazine = await this.findOne(model.id);
        if (!magazine) {
            throw new ApplicationException(400, 'Сannot update an magazine');
        }
        await this.update(model.id, model);
    }

    public async deleteMagazineById(id: string): Promise<void> {
        const magazine = await this.findOne(id);
        if (!magazine) {
            throw new ApplicationException(400, 'Сannot delete an magazine');
        }
        await this.delete(id);
    }

    public async getAll(): Promise<MagazineEntity[]> {
        const magazines = await this.find();
        magazines.sort(function(a, b) {
            const firstDate = new Date(a.creationDate);
            const secondDate = new Date(b.creationDate);
            return firstDate > secondDate ? -1 : firstDate < secondDate ? 1 : 0;
        });
        return magazines;
    }
 }