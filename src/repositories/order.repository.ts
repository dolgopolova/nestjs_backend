import { EntityRepository, Repository, MongoRepository } from "typeorm";
import { OrderEntity } from "../entities/orders.entity";
import { OrderReceiveModel } from "../models/orders/order.receive.model";
import { OrderModel } from "../models/orders/order.model";
import { ApplicationException } from "../common/exceptions/application.exception";

@EntityRepository(OrderEntity)
export class OrderRepository extends Repository<OrderEntity> {

    public async createOrder(model: OrderReceiveModel, date: string): Promise<void> {
        const order = this.create({
            creationDate: date,
            buyerId: model.buyerId,
            products: model.products
        });
        this.insert(order);
    }

    public async getOrderById(id: string): Promise<OrderModel> {
        const order = await this.findOne(id);
        if (!order) {
            throw new ApplicationException(400, 'Order not found');
        }
        return new OrderModel(order);
    }

    public async updateOrderById(model: OrderModel): Promise<void> {
        const order = await this.findOne(model.id);
        if (!order) {
            throw new ApplicationException(400, 'Сannot update an order');
        }
        await this.update(model.id, model);
    }

    public async deleteOrderById(id: string): Promise<void> {
        const order = await this.findOne(id);
        if (!order) {
            throw new ApplicationException(400, 'Сannot delete an order');
        }
        await this.delete(id);
    }

    public async getAllUserOrders(userId: string): Promise<OrderEntity[]> {
        const orders = await this.find({ buyerId: userId });
        if (!orders) {
            throw new ApplicationException(400, 'User does not have orders');
        }
        orders.sort(function(a, b) {
            const firstDate = new Date(a.creationDate);
            const secondDate = new Date(b.creationDate);
            return firstDate > secondDate ? -1 : firstDate < secondDate ? 1 : 0; 
        });
        return orders;
    };
}