import {EntityRepository, Repository} from "typeorm";
import {UserEntity, UserRole} from "../entities";
import { SignInAuthModel } from "../models";
import { Md5 } from "ts-md5";
import { ApplicationException } from "../common/exceptions/application.exception";

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {

    public async findUser(model: SignInAuthModel): Promise<UserEntity> {
        const user = await this.findOne({
            email: model.email,
            passwordHash: Md5.hashAsciiStr(model.password).toString()
        });
        if (!user) {
            throw new ApplicationException(401, 'Incorrect email or password.');
        }
        return user;
    }

    public async findUserByEmail(email: string): Promise<boolean> {
        const user = await this.findOne({ email: email})
        if (user) {
            throw new ApplicationException(400, 'User has already exist!');
        }
        return true;
    }

    public async getAll() :Promise<UserEntity[]> {
        return await this.find({ role: UserRole.CLIENT });
    }

}