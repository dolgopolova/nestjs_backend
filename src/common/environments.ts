export enum Environments {
    'Development',
    'Production'
}

export const environment = {
    production: false,
    clientUrl: 'http://localhost:8100'
}