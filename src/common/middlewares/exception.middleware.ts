import { ExceptionFilter, Catch, ArgumentsHost, HttpStatus, HttpException } from '@nestjs/common';
import { ApplicationException } from '../exceptions/application.exception';
import { Environments } from '../environments'
@Catch()
export class ExceptionHandlerFilter implements ExceptionFilter {
    catch(error: Error, host: ArgumentsHost) {
        var logger = require('logzio-nodejs').createLogger({
            token: 'PlstCoIDoKwqBslCYmLyXYyhBSjWLyVM',
            protocol: 'https',
            host: 'listener.logz.io',
            port: '8071'
        });
        let response = host.switchToHttp().getResponse();

        let status = (error instanceof ApplicationException) ?
            HttpStatus.BAD_REQUEST : HttpStatus.INTERNAL_SERVER_ERROR;

        if (status === HttpStatus.BAD_REQUEST) {
            return response.status(status).send(error.message).json({ message: error.message});
        }
        if (status === HttpStatus.INTERNAL_SERVER_ERROR) {
            if (process.env.NODE_ENV == Environments.Production.toString()) {
                var obj = {
                    message: error.message,
                    name: error.name,
                    stack: error.stack
                };
                logger.log(obj);
                return response.status(status).send('Internal Server Error!');
            }
            else {
                return response.status(status).send(error.message);
            }
        }
    }
}