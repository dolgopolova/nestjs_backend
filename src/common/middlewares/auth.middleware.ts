import { NestMiddleware, Injectable } from "@nestjs/common";
import { Request, Response } from 'express';
import { verify } from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: Function) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0].toLowerCase() === 'bearer') {
            const token = req.headers.authorization.split(' ')[1];
            verify(token, 'secretKey', (err, payload) => {
                if (!err) {
                    next();
                } else {
                    return res.status(401).json(err);
                }
            });
        } else {
            next();
        }
    }
}