import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserRole } from '../../entities/user.entity';
import { verify } from 'jsonwebtoken';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<UserRole[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    
    if (request.headers.hasOwnProperty('authorization'))
    {
      const token = request.headers.authorization.split(' ')[1];
      var user = verify(token, 'secretKey');
    }
    if(typeof user === 'object' && user.hasOwnProperty('role'))
    {
        var hasRole = roles.includes(user["role"]);
    }

    return user && hasRole;
  }
}