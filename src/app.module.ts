import "reflect-metadata"
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { BookModule } from "./modules/book.module";
import { MagazineModule } from "./modules/magazine.module";
import { ProductModule } from "./modules/product.module";
import { OrderModule } from "./modules/order.module";
import { AuthModule } from "./modules/auth.module";
import { AuthMiddleware } from "./common/middlewares/auth.middleware";
import { AuthController, BookController, MagazineController, OrderController, ProductController } from "./controllers";
import { PurchaseController } from "./controllers/purchase.controller";
import { PurchaseModule } from "./modules/purchase.module";

const controllers = [
  AuthController,
  BookController,
  MagazineController,
  OrderController,
  ProductController,
  PurchaseController
];

@Module({
  imports: [
    AuthModule, BookModule, MagazineModule, OrderModule, ProductModule, PurchaseModule
  ],
})
export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(...controllers);
  }
}
