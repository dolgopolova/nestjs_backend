import { Module } from '@nestjs/common';
import { AuthController } from '../controllers';
import { AuthService } from '../services';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { DatabaseModule } from './database.module';
import { EmailService } from '../services/email.service';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secretOrPrivateKey: 'secretKey',
      signOptions: {
        expiresIn: '1 day',
      },
    }),
    DatabaseModule
  ],
  controllers: [AuthController],
  providers: [AuthService, EmailService],
})
export class AuthModule {}