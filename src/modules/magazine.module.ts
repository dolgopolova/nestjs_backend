import { Module } from '@nestjs/common';
import { MagazineController } from '../controllers';
import { MagazineService } from '../services';
import { DatabaseModule } from './database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [MagazineController],
  providers: [MagazineService],
})
export class MagazineModule {}