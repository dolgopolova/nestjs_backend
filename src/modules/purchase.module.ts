import { Module } from '@nestjs/common';
import { DatabaseModule } from './database.module';
import { PurchaseController } from '../controllers/purchase.controller';
import { PurchaseService } from '../services/purchase.service';

@Module({
  imports: [DatabaseModule],
  controllers: [PurchaseController],
  providers: [PurchaseService],
})
export class PurchaseModule {
  
}