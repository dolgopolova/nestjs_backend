import { Module } from '@nestjs/common';
import { BookController } from '../controllers';
import { BookService } from '../services';
import { DatabaseModule } from './database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [BookController],
  providers: [BookService],
})
export class BookModule {
  
}