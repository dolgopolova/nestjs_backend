import { Module } from '@nestjs/common';
import { ProductController } from '../controllers';
import { ProductService } from '../services';
import { DatabaseModule } from './database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}