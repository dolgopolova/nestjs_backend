import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity, BookEntity, MagazineEntity, EntityBase } from '../entities';
import { OrderEntity } from '../entities/orders.entity';
import { PurchaseItemEntity } from '../entities/purchaseItem.entity';

const entities = [
  EntityBase,
  UserEntity, 
  BookEntity, 
  MagazineEntity, 
  OrderEntity,
  PurchaseItemEntity
];

@Global()
@Module({
  imports:[
    TypeOrmModule.forRoot({
      type: "mongodb",
      name: "TestDataBase",
      url: "mongodb+srv://TestMongoDBUser:myNewMongoDBUser@testdatabase-mrz6w.mongodb.net/BookMarket?retryWrites=true",
      port: 3000,
      username: "TestMongoDBUser",
      password: "myNewMongoDBUser",
      database: "BookMarket",
      entities: [...entities],
      synchronize: true,
      useNewUrlParser: true
    }),
    TypeOrmModule.forFeature( [...entities], "TestDataBase" ),
  ],
  exports: [
    TypeOrmModule.forRoot({
      type: "mongodb",
      name: "TestDataBase",
      url: "mongodb+srv://TestMongoDBUser:myNewMongoDBUser@testdatabase-mrz6w.mongodb.net/BookMarket?retryWrites=true",
      port: 3000,
      username: "TestMongoDBUser",
      password: "myNewMongoDBUser",
      database: "BookMarket",
      entities: [...entities],
      synchronize: true,
      useNewUrlParser: true
    }),
    TypeOrmModule.forFeature( [...entities], "TestDataBase" ),
  ],
})
export class DatabaseModule {}