import { ApiModelProperty } from "@nestjs/swagger";
import { BookModel } from "./books/book.model";
import { MagazineModel } from "./magazine/magazine.model";

export class ProductModel
{
    constructor(booksModel: BookModel[], magazinesModel: MagazineModel[])
    {
        this.books = booksModel;
        this.magazines = magazinesModel;
    }

    @ApiModelProperty()
    books: BookModel[];

    @ApiModelProperty()
    magazines: MagazineModel[];
}