
import { ApiModelProperty } from "@nestjs/swagger";
import { CategoryModel } from "../category.model";

export class MagazineReceiveModel
{
    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    publisher: string;

    @ApiModelProperty()
    year: number;

    @ApiModelProperty()
    price: number;

    @ApiModelProperty()
    category: CategoryModel;
}