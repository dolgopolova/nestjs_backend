import { MagazineEntity } from "../../entities/magazine.entity";
import { ApiModelProperty } from "@nestjs/swagger";
import { CategoryModel } from "../category.model";
import { ModelBase } from "../auth/base.model";

export class MagazineModel extends ModelBase
{
    constructor(entity: MagazineEntity)
    {
        super(entity.id.toString(), entity.creationDate);
        this.name = entity.name;
        this.publisher = entity.publisher;
        this.year = entity.year;
        this.price = entity.price;
        this.category = entity.category;
    }

    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    publisher: string;

    @ApiModelProperty()
    year: number;

    @ApiModelProperty()
    price: number;

    @ApiModelProperty()
    category: CategoryModel;
}