import { ApiModelProperty } from "@nestjs/swagger";
import { CategoryModel } from "../category.model";

export class BookReceiveModel
{
    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    authors: string;

    @ApiModelProperty()
    description: string;

    @ApiModelProperty()
    price: number;

    @ApiModelProperty()
    year: number;

    @ApiModelProperty()
    category: CategoryModel;
}