import { ApiModelProperty } from "@nestjs/swagger";
import { BookEntity } from "../../entities/book.entity";
import { CategoryModel } from "../category.model";
import { ModelBase } from "../auth/base.model";

export class BookModel extends ModelBase
{
    constructor(entity: BookEntity) 
    {
        super(entity.id.toString(), entity.creationDate);
        this.name = entity.name;
        this.description = entity.description;
        this.authors = entity.authors;
        this.price = entity.price;
        this.year = entity.year;
        this.category = entity.category;
    }

    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    authors: string;

    @ApiModelProperty()
    description: string;

    @ApiModelProperty()
    price: number;

    @ApiModelProperty()
    year: number;

    @ApiModelProperty()
    category: CategoryModel;
}