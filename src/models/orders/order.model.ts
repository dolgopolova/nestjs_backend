import { ApiModelProperty } from "@nestjs/swagger";
import { ProductModel } from "../poduct.model";
import { OrderEntity } from "../../entities/orders.entity";

export class OrderModel
{
    constructor(model: OrderEntity)
    {
        this.id = model.id.toString();
        this.creationDate = model.creationDate;
        this.buyerId = model.buyerId;
        this.products = model.products;
    }

    @ApiModelProperty()
    id: string;

    @ApiModelProperty()
    creationDate: string;

    @ApiModelProperty()
    buyerId: string;

    @ApiModelProperty()
    products: ProductModel;
}