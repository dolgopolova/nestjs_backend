import { ApiModelProperty } from "@nestjs/swagger";
import { ProductModel } from "../poduct.model";

export class OrderReceiveModel
{
    @ApiModelProperty()
    buyerId: string;

    @ApiModelProperty()
    products: ProductModel;
}