import { ApiModelProperty } from "@nestjs/swagger";
import { PurchaseItemEntity, ProductType } from "../../entities/purchaseItem.entity";

export class PurchaseItemModel {
    constructor(purchaseEntity: PurchaseItemEntity) {
        this.id = purchaseEntity.id.toString();
        this.creationDate = purchaseEntity.creationDate;
        this.name = purchaseEntity.name;
        this.userId = purchaseEntity.userId.toString();
        this.productId = purchaseEntity.productId.toString();
        this.qty = purchaseEntity.qty;
        this.type = purchaseEntity.type;
        this.costsPerOne = purchaseEntity.costsPerOne;
    }

    @ApiModelProperty()
    id: string;

    @ApiModelProperty()
    creationDate: string;

    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    userId: string;

    @ApiModelProperty()
    productId: string;

    @ApiModelProperty()
    qty: number;

    @ApiModelProperty()
    type: ProductType;

    @ApiModelProperty()
    costsPerOne: number;

}