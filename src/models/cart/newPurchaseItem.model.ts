import { ApiModelProperty } from "@nestjs/swagger";
import { PurchaseItemEntity, ProductType } from "../../entities/purchaseItem.entity";

export class NewPurchaseItemModel {
    @ApiModelProperty()
    name: string;

    @ApiModelProperty()
    userId: string;

    @ApiModelProperty()
    productId: string;

    @ApiModelProperty()
    qty: number;

    @ApiModelProperty()
    type: ProductType;

    @ApiModelProperty()
    costsPerOne: number;

}