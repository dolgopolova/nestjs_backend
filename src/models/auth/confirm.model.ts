import { ApiModelProperty } from "@nestjs/swagger";

export class ConfirmModel
{
    @ApiModelProperty()
    email: string;

    @ApiModelProperty()
    expiresIn: number;
}