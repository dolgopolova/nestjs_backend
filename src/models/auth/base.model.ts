import { ApiModelProperty } from "@nestjs/swagger";

export class ModelBase
{
    constructor(id: string, creationDate: string)
    {
        this.id = id;
        this.creationDate = creationDate;
    }

    @ApiModelProperty()
    id: string;

    @ApiModelProperty()
    creationDate: string;
}