import { ApiModelProperty } from '@nestjs/swagger';
export class TokenAuthAdminModel {

    @ApiModelProperty()
    expiresIn: number | string;

    @ApiModelProperty()
    accessToken: string;

    @ApiModelProperty()
    firstName: string;

    @ApiModelProperty()
    lastName: string;

    @ApiModelProperty()
    email: string;
}