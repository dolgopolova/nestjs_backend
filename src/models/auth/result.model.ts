import { ApiModelProperty } from '@nestjs/swagger';

export class ResultMessageModel {
    @ApiModelProperty()
    message: string;
}