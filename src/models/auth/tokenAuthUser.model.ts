import { ApiModelProperty } from '@nestjs/swagger';
export class TokenAuthUserModel {

    @ApiModelProperty()
    expiresIn: number | string;

    @ApiModelProperty()
    userId: string;

    @ApiModelProperty()
    accessToken: string;

    @ApiModelProperty()
    firstName: string;

    @ApiModelProperty()
    lastName: string;

    @ApiModelProperty()
    email: string;
}