import { ApiModelProperty } from "@nestjs/swagger";

export class CategoryModel {
    @ApiModelProperty()
    value: string;

    @ApiModelProperty()
    name: string;
}