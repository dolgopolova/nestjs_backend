import {Controller, Get, Post, Body, Delete, Put, Param, UseGuards} from '@nestjs/common'
import { BookService } from '../services';
import { BookModel } from '../models';
import { ApiResponse, ApiOkResponse, ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { BookReceiveModel } from '../models/books/book.receive.model';
import { UserRole } from '../entities';
import { Roles } from '../common';
import { RolesGuard } from '../common/guards/roles.guard';

@ApiBearerAuth()
@ApiUseTags('Book')
@Controller('api/book')
@UseGuards(RolesGuard)
export class BookController
{
    constructor(private booksService: BookService) {
    }

    @Post('create')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Create book", operationId: "createBook"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async createBook(@Body() model: BookReceiveModel): Promise<boolean>{
        return await this.booksService.createBook(model);
    }

    @Get('get/:id')
    @ApiOperation({title: "Get book by book id", operationId: "getBook"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: BookModel})
    async getBookById(@Param('id') id: string): Promise<BookModel>{
        return await this.booksService.getBookById(id);
    }

    @Put('update')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Update book data", operationId: "updateBook"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async updateBook(@Body() model: BookModel): Promise<boolean>{
        return await this.booksService.updateBook(model);
    }

    @Delete('delete/:id')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Delete book by id", operationId: "deleteBook"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async deleteBookById(@Param('id') id: string): Promise<boolean>{
        return await this.booksService.deleteBookById(id);
    }

    @Get('getAll')
    @ApiOperation({title: "Get all books in the system", operationId: "getAllBooks"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: BookModel, isArray: true })
    async getAll(): Promise<BookModel[]>{
        return await this.booksService.getAll();
    }
}