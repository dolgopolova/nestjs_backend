import {Controller, Get, Post, Body, Delete, Put, Param, UseGuards} from '@nestjs/common'
import { ApiResponse, ApiOkResponse, ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { OrderReceiveModel } from '../models/orders/order.receive.model';
import { OrderService } from '../services/order.service';
import { OrderModel } from '../models/orders/order.model';
import { UserRole } from '../entities';
import { Roles } from '../common';
import { RolesGuard } from '../common/guards/roles.guard';

@ApiBearerAuth()
@ApiUseTags('Basket')
@Controller('api/basket')
@UseGuards(RolesGuard)
export class OrderController
{
    constructor(private readonly basketService: OrderService) {}

    @Post('create/order')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Create order", operationId: "createOrder"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async createOrder(@Body() model: OrderReceiveModel): Promise<boolean>{
        return await this.basketService.createOrder(model);
    }

    @Get('get/orderById/:id')
    @Roles(UserRole.CLIENT, UserRole.ADMIN)
    @ApiOperation({title: "Get order by its id", operationId: "getOrder"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: OrderModel})
    async getOrderById(@Param('id') id: string): Promise<OrderModel>{
        return await this.basketService.getOrderById(id);
    }

    @Put('update/order')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Update orders data", operationId: "updateOrder"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async updateOrderById(@Body() model: OrderModel): Promise<boolean>{
        return await this.basketService.updateOrderById(model);
    }

    @Delete('delete/order/:id')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Delete order by its id", operationId: "deleteOrder"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async deleteOrderById(@Param('id') id: string): Promise<boolean>{
        return await this.basketService.deleteOrderById(id);
    }

    @Get('getAllUserOrders/:id')
    @Roles(UserRole.CLIENT, UserRole.ADMIN)
    @ApiOperation({title: "Get all user orders by user id", operationId: "getAllOrders"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: OrderModel, isArray: true })
    async getAllUserOrders(@Param('id') id: string): Promise<OrderModel[]>{
        return await this.basketService.getAllUserOrders(id);
    }
}