import { ApiBearerAuth, ApiUseTags, ApiOperation, ApiResponse, ApiOkResponse } from "@nestjs/swagger";
import { Controller, UseGuards, Put, Body, Get, Post, Param, Query, Delete } from "@nestjs/common";
import { RolesGuard } from "../common/guards/roles.guard";
import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { PurchaseService } from "../services/purchase.service";
import { Roles } from "../common";
import { UserRole } from "../entities";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";

@ApiBearerAuth()
@ApiUseTags('Purchase')
@Controller('/api/purchase')
@UseGuards(RolesGuard)
export class PurchaseController {

    constructor(private readonly cartService: PurchaseService) {
    }

    @Post('createPurchase')
    @Roles(UserRole.CLIENT)
    @ApiOperation({title: "Creates a cart that will be tied to a specific user.", operationId: "createPurchase"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    public async createPurchase(@Body() userPurchase: NewPurchaseItemModel): Promise<boolean> {
        return await this.cartService.createPurchase(userPurchase);
    }

    @Get('getPurchaseByUserId/:userId')
    @Roles(UserRole.CLIENT)
    @ApiOperation({title: "Returns items that are in the user’s basket.", operationId: "getPurchaseByUserId"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: PurchaseItemModel, isArray: true })
    public async getPurchaseByUserId(@Param('userId') userId: string): Promise<PurchaseItemModel[]> {
        return await this.cartService.getPurchaseByUserId(userId);
    }

    @Put('updatePurchase')
    @Roles(UserRole.CLIENT)
    @ApiOperation({title: "Updates the cart on the id of the user to whom it belongs. If the cart is not in the database, it is created", operationId: "updatePurchaseByUserId"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    public async updateUsersPurchase(@Body() userPurchase: PurchaseItemModel): Promise<boolean> {
        return await this.cartService.updatePurchaseByUserId(userPurchase);
    }

    @Get('findPurchaseByUserIdProductId')
    @Roles(UserRole.CLIENT)
    @ApiOperation({title: "Finds an existing purchase by user ID and product ID", operationId: "findPurchaseByUserIdProductId"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    public async findPurchase(@Query('userId') userId: string, @Query('productId') productId: string): Promise<PurchaseItemModel> {
        return await this.cartService.findPurchaseByUserIdProductId(userId, productId);
    }

    @Delete('delete/:purchaseId')
    @Roles(UserRole.CLIENT)
    @ApiOperation({title: "Finds an existing purchase by user ID and product ID", operationId: "findPurchaseByUserIdProductId"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    public async deletePurchase(@Param('purchaseId') purchaseId: string): Promise<boolean> {
        return await this.cartService.deletePurchaseByUserIdProductId(purchaseId);
    }
}