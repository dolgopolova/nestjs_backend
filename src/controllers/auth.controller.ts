import { Controller, Body, Post, Get, UseGuards, Param } from '@nestjs/common';
import { AuthService } from '../services';
import { TokenAuthAdminModel, SignUpAuthModel, UserModel } from '../models'
import {
  ApiBearerAuth,
  ApiUseTags,
  ApiOkResponse,
  ApiResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { SignInAuthModel } from '../models/auth/signIn.model';
import { ResultMessageModel } from '../models/auth/result.model';
import { Roles } from '../common';
import { UserRole } from '../entities';
import { RolesGuard } from '../common/guards/roles.guard';
import { TokenAuthUserModel } from '../models/auth/tokenAuthUser.model';

@ApiBearerAuth()
@ApiUseTags('Auth')
@Controller('/api/auth')
@UseGuards(RolesGuard)
export class AuthController {
  
  constructor(private authService: AuthService) {
   }

  @Post('signIn')
  @ApiOperation({title: "User sign in through mail", operationId: "signIn"})
  @ApiOkResponse({ type: TokenAuthUserModel })
  async signIn(@Body() model: SignInAuthModel): Promise<TokenAuthUserModel> {
    return await this.authService.signIn(model);
  }

  @Post('signInAdmin')
  @ApiOperation({title: "Administrator sign in through mail", operationId: "signInAdmin"})
  @ApiOkResponse({ type: TokenAuthAdminModel })
  async signInAdmin(@Body() model: SignInAuthModel): Promise<TokenAuthAdminModel> {
    return await this.authService.signInAdmin(model);
  }

  @Post('signUp')
  @ApiOperation({title: "User registration", operationId: "signUp"})
  @ApiOkResponse({ type: ResultMessageModel })
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  async signUp(@Body() model: SignUpAuthModel): Promise<ResultMessageModel> {
    return await this.authService.signUp(model);
  }

  @Get('users/getAll')
  @Roles(UserRole.ADMIN)
  @ApiOperation({title: "Get all users in the system", operationId: "getAll"})
  @ApiOkResponse({ type: UserModel, isArray: true })
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  async getAll(): Promise<UserModel[]>{
    return await this.authService.getAll();
  }

  @Get('confirmation/:emailToken')
  @ApiOperation({title: "By this url user can confirm his registration email", operationId: "confirm"})
  @ApiOkResponse({ type: ResultMessageModel })
  @ApiResponse({ status: 200, description: 'Success'})
  @ApiResponse({ status: 400, description: 'Bad Request'})
  async confirmEmail(@Param('emailToken') emailToken: string): Promise<ResultMessageModel> {
    return await this.authService.confirmEmail(emailToken);
  }
}
