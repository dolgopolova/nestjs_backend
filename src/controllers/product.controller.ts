import { Controller, Get } from '@nestjs/common'
import { ApiResponse, ApiOkResponse, ApiUseTags, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';
import { ProductService } from '../services';
import { ProductModel } from '../models/poduct.model';

@ApiBearerAuth()
@ApiUseTags('Products')
@Controller('api/products')
export class ProductController
{
    constructor(private readonly productsService: ProductService) {}

    @Get('getAll')
    @ApiOperation({title: "Get all products in the system", operationId: "getAllProducts"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: ProductModel, isArray: true })
    async getAll(): Promise<ProductModel>{
        return await this.productsService.getAll();
    }
}