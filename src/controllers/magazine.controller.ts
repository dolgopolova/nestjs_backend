import { ApiUseTags, ApiResponse, ApiOkResponse, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";
import { Controller, Post, Body, Get, Put, Delete, Param, UseGuards } from "@nestjs/common";
import { MagazineService } from "../services";
import { MagazineModel } from "../models";
import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
import { UserRole } from "../entities";
import { Roles } from "../common";
import { RolesGuard } from "../common/guards/roles.guard";

@ApiBearerAuth()
@ApiUseTags('Magazine')
@Controller('api/magazine')
@UseGuards(RolesGuard)
export class MagazineController
{
    constructor(private magazineService: MagazineService) {
    }

    @Post('create')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Create magazine", operationId: "createMagazine"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async createMagazine(@Body() model: MagazineReceiveModel): Promise<boolean>{
        return await this.magazineService.createMagazine(model);
    }

    @Get('get/:id')
    @ApiOperation({title: "Get magazine by its id", operationId: "getMagazine"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: MagazineModel})
    async getMagazineById(@Param('id') id: string): Promise<MagazineModel>{
        return await this.magazineService.getMagazineById(id);
    }

    @Put('update')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Update magazine data", operationId: "updateMagazine"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async updateMagazineById(@Body() model: MagazineModel): Promise<boolean>{
        return await this.magazineService.updateMagazineById(model);
    }

    @Delete('delete/:id')
    @Roles(UserRole.ADMIN)
    @ApiOperation({title: "Delete magazine by its id", operationId: "deleteMagazine"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    async deleteMagazineById(@Param('id') id: string): Promise<boolean>{
        return await this.magazineService.deleteMagazineById(id);
    }

    @Get('getAll')
    @ApiOperation({title: "Get all magazines in the system", operationId: "getAllMagazines"})
    @ApiResponse({ status: 200, description: 'Success'})
    @ApiResponse({ status: 400, description: 'Bad Request'})
    @ApiOkResponse({ type: MagazineModel, isArray: true })
    async getAll(): Promise<MagazineModel[]>{
        return await this.magazineService.getAll();
    }
}