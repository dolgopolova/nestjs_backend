import { Column, Entity } from 'typeorm';
import { EntityBase } from './base.entity';
import { CategoryModel } from '../models/category.model';

@Entity({name: 'Books'})
export class BookEntity extends EntityBase
{
  @Column({ length: 500 })
  name: string;

  @Column()
  description: string;

  @Column({length : 200})
  authors: string;

  @Column()
  price: number;

  @Column()
  year: number;

  @Column()
  category: CategoryModel;
}
