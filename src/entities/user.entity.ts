import { Column, Entity, ObjectIdColumn, ObjectID } from 'typeorm';

export enum UserRole {
    ADMIN = "admin",
    CLIENT = "user"
}

@Entity({name: 'Users'})
export class UserEntity {
    @ObjectIdColumn()
    id: ObjectID;

    @Column({ length: 500 })
    email: string;

    @Column({
        default: false
    })
    isEmailConfirmed: boolean;

    @Column({ length: 500 })
    firstName: string;

    @Column({ length: 500 })
    lastName: string;

    @Column({ length: 500 })
    passwordHash: string;

    @Column({
        type: "enum",
        enum: UserRole,
        default: UserRole.CLIENT
    })
    role: UserRole
}
