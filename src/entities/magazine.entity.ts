import { Column, Entity } from 'typeorm';
import { EntityBase } from './base.entity';
import { CategoryModel } from '../models/category.model';

@Entity({name: 'Magazines'})
export class MagazineEntity extends EntityBase
{
  @Column({ length: 500 })
  name: string;

  @Column({length: 200})
  publisher: string;

  @Column()
  year: number;

  @Column()
  price: number;

  @Column()
  category: CategoryModel;
}
