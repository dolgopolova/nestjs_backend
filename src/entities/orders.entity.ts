import { Column, Entity } from 'typeorm';
import { EntityBase } from './base.entity';
import { ProductModel } from '../models/poduct.model';

@Entity({name: 'Orders'})
export class OrderEntity extends EntityBase
{
  @Column()
  buyerId: string;

  @Column()
  products: ProductModel;
}

