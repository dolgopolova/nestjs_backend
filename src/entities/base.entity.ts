import { Entity, ObjectIdColumn, ObjectID, Column } from "typeorm";

@Entity({name: 'Base'})
export class EntityBase
{
    @ObjectIdColumn({type: 'string'})
    id: ObjectID;

    @Column()
    creationDate: string;
}