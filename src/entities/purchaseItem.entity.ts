import { Column, Entity, ObjectID } from 'typeorm';
import { EntityBase } from './base.entity';

export enum ProductType {
  BOOK = "book",
  MAGAZINE = "magazine"
}

@Entity({ name: 'PurchaseItems' })
export class PurchaseItemEntity extends EntityBase {
  @Column({ type: 'string' })
  userId: ObjectID;

  @Column()
  creationDate: string;

  @Column()
  name: string;

  @Column({ type: 'string' })
  productId: ObjectID;

  @Column({ type: 'number' })
  qty: number;

  @Column()
  type: ProductType;

  @Column({ type: 'number' })
  costsPerOne: number;
}