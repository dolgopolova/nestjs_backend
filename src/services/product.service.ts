import {Injectable} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm';
import { BookEntity, MagazineEntity } from '../entities';
import { BookRepository } from '../repositories/book.repository';
import { ProductModel } from '../models/poduct.model';
import { MagazineRepository } from '../repositories/magazine.repository';
import { BookModel, MagazineModel } from '../models';

@Injectable()
export class ProductService
{
    constructor(@InjectRepository(BookEntity) private readonly booksRepository: BookRepository,
    @InjectRepository(MagazineEntity) private readonly magazineRepository: MagazineRepository) { }

    async getAll(): Promise<ProductModel> {
        const books = await this.booksRepository.find();
        const magazines = await this.magazineRepository.find();
        books.sort(function(a, b) {
            var textA = a.name.toLowerCase();
            var textB = b.name.toLowerCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0; 
        });
        magazines.sort(function(a, b) {
            var textA = a.name.toLowerCase();
            var textB = b.name.toLowerCase();
            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0; 
        });

        const booksModel = books.map(book => { return new BookModel(book)});
        const magazineModel = magazines.map(magazine => {return new MagazineModel(magazine)});

        return new ProductModel(booksModel, magazineModel);
    }
}