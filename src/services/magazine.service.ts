import { Injectable, Scope } from "@nestjs/common";
import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
import { MagazineModel } from "../models";
import { MagazineRepository } from "../repositories/magazine.repository";
import { getCustomRepository } from "typeorm";

@Injectable({
    scope: Scope.REQUEST
})
export class MagazineService {
    private readonly magazineRepository: MagazineRepository;

    constructor() {
        this.magazineRepository = getCustomRepository(MagazineRepository, "TestDataBase");
    }

    public async createMagazine(model: MagazineReceiveModel): Promise<boolean> {
        const dateTime = this.getDate();
        this.magazineRepository.createMagazine(model, dateTime);
        return true;
    }

    public async getMagazineById(id: string): Promise<MagazineModel> {
        const magazine = await this.magazineRepository.getMagazineById(id);
        return new MagazineModel(magazine);
    }

    public async updateMagazineById(model: MagazineModel): Promise<boolean> {
        const dateTime = this.getDate();
        model.creationDate = dateTime;
        await this.magazineRepository.updateMagazine(model);
        return true;
    }

    public async deleteMagazineById(id: string): Promise<boolean> {
        await this.magazineRepository.deleteMagazineById(id);
        return true;
    }

    public async getAll(): Promise<MagazineModel[]> {
        const magazines = await this.magazineRepository.getAll();
        return magazines.map(magazine => {
            return new MagazineModel(magazine);
        });
    }

    private getDate() : string {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return date + ' ' + time;
    }
}