import {Injectable, Scope} from '@nestjs/common'
import { BookModel } from '../models/books/book.model';
import { BookRepository } from '../repositories/book.repository';
import { BookReceiveModel } from '../models/books/book.receive.model';
import { getCustomRepository } from 'typeorm';

@Injectable({
    scope: Scope.REQUEST
})
export class BookService
{
    private readonly bookRepository: BookRepository;

    constructor() {
        this.bookRepository = getCustomRepository(BookRepository, "TestDataBase");
    }

    public async createBook(model: BookReceiveModel): Promise<boolean> {
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        await this.bookRepository.createBook(model, dateTime);
        return true;
    }

    public async getBookById(id: string): Promise<BookModel> {
        const book = await this.bookRepository.getBookById(id);
        return new BookModel(book);
    }

    public async updateBook(model: BookModel): Promise<boolean> {
        const dateTime = this.getDate();
        model.creationDate = dateTime;
        await this.bookRepository.updateBook(model);
        return true;
    }

    public async deleteBookById(id: string): Promise<boolean> {
        await this.bookRepository.deleteBookById(id);
        return true;
    }

    public async getAll(): Promise<BookModel[]> {
        const books = await this.bookRepository.getAll();
        return books.map(book =>{
            return new BookModel(book);
        });
    }

    private getDate() : string {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return date + ' ' + time;
    }
}