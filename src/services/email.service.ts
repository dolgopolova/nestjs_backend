import { ApplicationException } from "../common/exceptions/application.exception";
import { environment } from "../common";
import { sign } from "jsonwebtoken";
import { Injectable } from "@nestjs/common";

@Injectable()
export class EmailService {

    public sendMail(toEmail: string) {
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({ 
            host: 'smtp.gmail.com', 
            auth: {
                type: "login",
                user: "nestmarketcreator@gmail.com",
                pass: "Qweasdzx123"
              }
        });
        const emailToken = sign({
            email: toEmail
        }, 'secretKey', { expiresIn: '1d'});

        const url = `${environment.clientUrl}/account/confirmation/${emailToken}`;
        var mailOptions = { 
            from: 'market@nestmarket.com', 
            to: toEmail, 
            subject: 'Account Verification', 
            html: `Hello,\n\n Please verify your account by clicking the link: <a href='${url}'>${url}</a>` };
            transporter.sendMail(mailOptions, function (err) {
                if (err) { 
                    return new ApplicationException(500, err.message); 
                }
            });
    }
}