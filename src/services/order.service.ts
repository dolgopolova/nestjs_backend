import { Injectable, Scope } from "@nestjs/common";
import { OrderRepository } from "../repositories/order.repository";
import { OrderModel } from "../models/orders/order.model";
import { OrderReceiveModel } from "../models/orders/order.receive.model";
import { getCustomRepository } from "typeorm";

@Injectable({
    scope: Scope.REQUEST
})
export class OrderService {
    private readonly ordersRepository: OrderRepository;

    constructor() {
        this.ordersRepository = getCustomRepository(OrderRepository, "TestDataBase");
    }

    public async createOrder(model: OrderReceiveModel): Promise<boolean> {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        await this.ordersRepository.createOrder(model, dateTime);

        return true;
    }

    public async getOrderById(id: string): Promise<OrderModel> {
        return this.ordersRepository.getOrderById(id);
    }

    public async updateOrderById(model: OrderModel): Promise<boolean> {
        await this.ordersRepository.updateOrderById(model);
        return true;
    }

    public async deleteOrderById(id: string): Promise<boolean> {
        await this.ordersRepository.deleteOrderById(id);
        return true;
    }

    public async getAllUserOrders(id: string): Promise<OrderModel[]> {
        const orders = await this.ordersRepository.getAllUserOrders(id);
        
        return orders.map(order =>{
            return new OrderModel(order);
        });
    }

}