import { Injectable, Scope } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenAuthAdminModel, SignUpAuthModel, UserModel } from '../models';
import { SignInAuthModel } from '../models/auth/signIn.model';
import { UserRole, UserEntity } from '../entities/user.entity';
import { Md5 } from 'ts-md5/dist/md5';
import { UserRepository } from '../repositories/user.repository';
import { ResultMessageModel } from '../models/auth/result.model';
import { getCustomRepository } from 'typeorm';
import { ApplicationException } from '../common/exceptions/application.exception';
import { verify } from 'jsonwebtoken';
import { ConfirmModel } from '../models/auth/confirm.model';
import { EmailService } from './email.service';
import { TokenAuthUserModel } from '../models/auth/tokenAuthUser.model';

@Injectable({
    scope: Scope.REQUEST
})
export class AuthService {

    private userRepository: UserRepository;

    constructor(private readonly jwtService: JwtService, private readonly emailService: EmailService) {
        this.userRepository = getCustomRepository(UserRepository, "TestDataBase");
    }

    public async signIn(model: SignInAuthModel): Promise<TokenAuthUserModel> {
        const user = await this.userRepository.findUser(model);

        if (!user.isEmailConfirmed) {
            throw new ApplicationException(400, 'Please confirm your email to login');
        }

        let accessToken: string = null;
        if (user.role == UserRole.CLIENT) {
            accessToken = this.jwtService.sign({
                email: user.email,
                role: user.role,
                name: user.firstName,
                surname: user.lastName
            }, { expiresIn: '1 day' });
        }
        return {
            expiresIn: '1 day',
            accessToken,
            userId: user.id.toString(),
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email
        };
    }

    public async signInAdmin(model: SignInAuthModel): Promise<TokenAuthAdminModel> {
        const user = await this.userRepository.findUser(model);
        let accessToken: string = null;
        if (user.role == UserRole.ADMIN) {
            accessToken = this.jwtService.sign({
                email: user.email,
                role: user.role,
                name: user.firstName,
                surname: user.lastName
            }, { expiresIn: '1 day' });
        }

        return {
            expiresIn: '1 day',
            accessToken,
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email
        };
    }

    public async signUp(model: SignUpAuthModel): Promise<ResultMessageModel> {
        await this.userRepository.findUserByEmail(model.email);
        const passwordHash = Md5.hashAsciiStr(model.password);
        let newUser = this.userRepository.create({
            email: model.email,
            isEmailConfirmed: false,
            firstName: model.firstName,
            lastName: model.lastName,
            passwordHash: passwordHash.toString(),
            role: UserRole.CLIENT
        });
        this.userRepository.insert(newUser);

        this.emailService.sendMail(newUser.email);

        return {
            message: "You will receive an email with an account confirmation."
        };
    }

    public async getAll(): Promise<UserModel[]> {
        const users = await this.userRepository.getAll();
        return users.map(user => {
            return new UserModel(user);
        });
    }

    public async confirmEmail(emailToken: string) : Promise<ResultMessageModel> {
        const confirm: ConfirmModel = verify(emailToken, 'secretKey') as ConfirmModel;
        if (!confirm)
        {
            throw new ApplicationException(400, 'Verification timed out.');
        }
        const user = await this.userRepository.findOne({ email: confirm.email})
        if (!user)
        {
            throw new ApplicationException(400, 'User has not exist!');
        }
        user.isEmailConfirmed = true;
        await this.userRepository.save<UserEntity>(user);
        return {
            message: "Email is successfully confirmed"
        };
    }
}
