import { Injectable, Scope } from "@nestjs/common";
import { getCustomRepository } from "typeorm";
import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { PurchaseRepository } from "../repositories/purchase.repository";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";

@Injectable({
    scope: Scope.REQUEST
})
export class PurchaseService {
    private cartRepository: PurchaseRepository;

    constructor() {
        this.cartRepository = getCustomRepository(PurchaseRepository, "TestDataBase");
    }

    public async createPurchase(userPurchase: NewPurchaseItemModel): Promise<boolean> {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        return await this.cartRepository.createPurchase(userPurchase, dateTime);
    }

    public async getPurchaseByUserId(id: string): Promise<PurchaseItemModel[]> {
        const cart = await this.cartRepository.getPurchaseByUserId(id);
        return cart.map(purchase =>{
            return new PurchaseItemModel(purchase);
        });
    }

    public async updatePurchaseByUserId(usersPurchase: PurchaseItemModel): Promise<boolean> {
        return await this.cartRepository.updatePurchaseByUserIdProductId(usersPurchase);
    }

    public async findPurchaseByUserIdProductId(userId: string, productId: string): Promise<PurchaseItemModel> {
        const purchase = await this.cartRepository.findPurchaseByUserIdProductId(userId, productId);
        return new PurchaseItemModel(purchase);
    }

    public async deletePurchaseByUserIdProductId(purchaseId: string): Promise<boolean> {
        await this.cartRepository.deletePurchaseByUserIdProductId(purchaseId);
        return true;
    }
}