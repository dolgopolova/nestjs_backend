"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./user.entity"));
__export(require("./base.entity"));
__export(require("./book.entity"));
__export(require("./magazine.entity"));
//# sourceMappingURL=index.js.map