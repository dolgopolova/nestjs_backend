import { EntityBase } from './base.entity';
import { CategoryModel } from '../models/category.model';
export declare class MagazineEntity extends EntityBase {
    name: string;
    publisher: string;
    year: number;
    price: number;
    category: CategoryModel;
}
