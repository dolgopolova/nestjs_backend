import { ObjectID } from 'typeorm';
import { EntityBase } from './base.entity';
export declare enum ProductType {
    BOOK = "book",
    MAGAZINE = "magazine"
}
export declare class PurchaseItemEntity extends EntityBase {
    userId: ObjectID;
    creationDate: string;
    name: string;
    productId: ObjectID;
    qty: number;
    type: ProductType;
    costsPerOne: number;
}
