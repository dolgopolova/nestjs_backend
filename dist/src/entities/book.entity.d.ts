import { EntityBase } from './base.entity';
import { CategoryModel } from '../models/category.model';
export declare class BookEntity extends EntityBase {
    name: string;
    description: string;
    authors: string;
    price: number;
    year: number;
    category: CategoryModel;
}
