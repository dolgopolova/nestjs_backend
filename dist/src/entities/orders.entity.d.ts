import { EntityBase } from './base.entity';
import { ProductModel } from '../models/poduct.model';
export declare class OrderEntity extends EntityBase {
    buyerId: string;
    products: ProductModel;
}
