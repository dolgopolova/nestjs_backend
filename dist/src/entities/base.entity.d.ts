import { ObjectID } from "typeorm";
export declare class EntityBase {
    id: ObjectID;
    creationDate: string;
}
