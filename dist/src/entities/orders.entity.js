"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const base_entity_1 = require("./base.entity");
const poduct_model_1 = require("../models/poduct.model");
let OrderEntity = class OrderEntity extends base_entity_1.EntityBase {
};
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], OrderEntity.prototype, "buyerId", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", poduct_model_1.ProductModel)
], OrderEntity.prototype, "products", void 0);
OrderEntity = __decorate([
    typeorm_1.Entity({ name: 'Orders' })
], OrderEntity);
exports.OrderEntity = OrderEntity;
//# sourceMappingURL=orders.entity.js.map