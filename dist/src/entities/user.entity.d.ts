import { ObjectID } from 'typeorm';
export declare enum UserRole {
    ADMIN = "admin",
    CLIENT = "user"
}
export declare class UserEntity {
    id: ObjectID;
    email: string;
    isEmailConfirmed: boolean;
    firstName: string;
    lastName: string;
    passwordHash: string;
    role: UserRole;
}
