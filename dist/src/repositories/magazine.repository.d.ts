import { Repository } from "typeorm";
import { MagazineEntity } from "../entities";
import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
import { MagazineModel } from "../models";
export declare class MagazineRepository extends Repository<MagazineEntity> {
    createMagazine(model: MagazineReceiveModel, dateTime: string): Promise<void>;
    getMagazineById(id: string): Promise<MagazineEntity>;
    updateMagazine(model: MagazineModel): Promise<void>;
    deleteMagazineById(id: string): Promise<void>;
    getAll(): Promise<MagazineEntity[]>;
}
