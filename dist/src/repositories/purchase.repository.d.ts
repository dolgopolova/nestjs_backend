import { Repository } from "typeorm";
import { PurchaseItemEntity } from "../entities/purchaseItem.entity";
import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";
export declare class PurchaseRepository extends Repository<PurchaseItemEntity> {
    createPurchase(purchase: NewPurchaseItemModel, dateTime: string): Promise<boolean>;
    updatePurchaseByUserIdProductId(usersPurchase: PurchaseItemModel): Promise<boolean>;
    getPurchaseByUserId(id: string): Promise<PurchaseItemEntity[]>;
    findPurchaseByUserIdProductId(userId: string, productId: string): Promise<PurchaseItemEntity>;
    deletePurchaseByUserIdProductId(purchaseId: string): Promise<boolean>;
}
