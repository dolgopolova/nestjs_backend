import { Repository } from "typeorm";
import { OrderEntity } from "../entities/orders.entity";
import { OrderReceiveModel } from "../models/orders/order.receive.model";
import { OrderModel } from "../models/orders/order.model";
export declare class OrderRepository extends Repository<OrderEntity> {
    createOrder(model: OrderReceiveModel, date: string): Promise<void>;
    getOrderById(id: string): Promise<OrderModel>;
    updateOrderById(model: OrderModel): Promise<void>;
    deleteOrderById(id: string): Promise<void>;
    getAllUserOrders(userId: string): Promise<OrderEntity[]>;
}
