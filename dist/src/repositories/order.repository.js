"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const orders_entity_1 = require("../entities/orders.entity");
const order_model_1 = require("../models/orders/order.model");
const application_exception_1 = require("../common/exceptions/application.exception");
let OrderRepository = class OrderRepository extends typeorm_1.Repository {
    createOrder(model, date) {
        return __awaiter(this, void 0, void 0, function* () {
            const order = this.create({
                creationDate: date,
                buyerId: model.buyerId,
                products: model.products
            });
            this.insert(order);
        });
    }
    getOrderById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const order = yield this.findOne(id);
            if (!order) {
                throw new application_exception_1.ApplicationException(400, 'Order not found');
            }
            return new order_model_1.OrderModel(order);
        });
    }
    updateOrderById(model) {
        return __awaiter(this, void 0, void 0, function* () {
            const order = yield this.findOne(model.id);
            if (!order) {
                throw new application_exception_1.ApplicationException(400, 'Сannot update an order');
            }
            yield this.update(model.id, model);
        });
    }
    deleteOrderById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const order = yield this.findOne(id);
            if (!order) {
                throw new application_exception_1.ApplicationException(400, 'Сannot delete an order');
            }
            yield this.delete(id);
        });
    }
    getAllUserOrders(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const orders = yield this.find({ buyerId: userId });
            if (!orders) {
                throw new application_exception_1.ApplicationException(400, 'User does not have orders');
            }
            orders.sort(function (a, b) {
                const firstDate = new Date(a.creationDate);
                const secondDate = new Date(b.creationDate);
                return firstDate > secondDate ? -1 : firstDate < secondDate ? 1 : 0;
            });
            return orders;
        });
    }
    ;
};
OrderRepository = __decorate([
    typeorm_1.EntityRepository(orders_entity_1.OrderEntity)
], OrderRepository);
exports.OrderRepository = OrderRepository;
//# sourceMappingURL=order.repository.js.map