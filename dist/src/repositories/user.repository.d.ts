import { Repository } from "typeorm";
import { UserEntity } from "../entities";
import { SignInAuthModel } from "../models";
export declare class UserRepository extends Repository<UserEntity> {
    findUser(model: SignInAuthModel): Promise<UserEntity>;
    findUserByEmail(email: string): Promise<boolean>;
    getAll(): Promise<UserEntity[]>;
}
