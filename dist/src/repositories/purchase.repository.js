"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const purchaseItem_entity_1 = require("../entities/purchaseItem.entity");
const purchaseItem_model_1 = require("../models/cart/purchaseItem.model");
const application_exception_1 = require("../common/exceptions/application.exception");
let PurchaseRepository = class PurchaseRepository extends typeorm_1.Repository {
    createPurchase(purchase, dateTime) {
        return __awaiter(this, void 0, void 0, function* () {
            const cart = yield this.findPurchaseByUserIdProductId(purchase.userId, purchase.productId);
            if (cart) {
                cart.qty += 1;
                yield this.updatePurchaseByUserIdProductId(new purchaseItem_model_1.PurchaseItemModel(cart));
                return true;
            }
            const newPurchase = yield this.create({
                creationDate: dateTime,
                name: purchase.name,
                userId: purchase.userId,
                productId: purchase.productId,
                qty: purchase.qty,
                type: purchase.type,
                costsPerOne: purchase.costsPerOne
            });
            yield this.insert(newPurchase);
            return true;
        });
    }
    updatePurchaseByUserIdProductId(usersPurchase) {
        return __awaiter(this, void 0, void 0, function* () {
            const purchase = yield this.findOne({ userId: usersPurchase.userId, productId: usersPurchase.productId });
            if (!purchase) {
                throw new application_exception_1.ApplicationException(400, 'Purchase was not found');
            }
            yield this.update(usersPurchase.id, usersPurchase);
            return true;
        });
    }
    getPurchaseByUserId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const purchase = yield this.find({ userId: id });
            return purchase;
        });
    }
    findPurchaseByUserIdProductId(userId, productId) {
        return __awaiter(this, void 0, void 0, function* () {
            const purchase = yield this.findOne({ userId: userId, productId: productId });
            return purchase;
        });
    }
    deletePurchaseByUserIdProductId(purchaseId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.delete(purchaseId);
            return true;
        });
    }
};
PurchaseRepository = __decorate([
    typeorm_1.EntityRepository(purchaseItem_entity_1.PurchaseItemEntity)
], PurchaseRepository);
exports.PurchaseRepository = PurchaseRepository;
//# sourceMappingURL=purchase.repository.js.map