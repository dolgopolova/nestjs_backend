"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const book_entity_1 = require("../entities/book.entity");
const application_exception_1 = require("../common/exceptions/application.exception");
let BookRepository = class BookRepository extends typeorm_1.Repository {
    createBook(model, dateTime) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.create({
                creationDate: dateTime,
                name: model.name,
                authors: model.authors,
                description: model.description,
                price: model.price,
                year: model.year,
                category: model.category
            });
            yield this.insert(book);
        });
    }
    getBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.findOne(id);
            if (!book) {
                throw new application_exception_1.ApplicationException(400, 'Book not found');
            }
            return book;
        });
    }
    updateBook(model) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.findOne(model.id);
            if (!book) {
                throw new application_exception_1.ApplicationException(400, 'Сannot update an book');
            }
            yield this.update(model.id, model);
        });
    }
    deleteBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.findOne(id);
            if (!book) {
                throw new application_exception_1.ApplicationException(400, 'Сannot delete an book');
            }
            yield this.delete(id);
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const books = yield this.find();
            books.sort(function (a, b) {
                const firstDate = new Date(a.creationDate);
                const secondDate = new Date(b.creationDate);
                return firstDate > secondDate ? -1 : firstDate < secondDate ? 1 : 0;
            });
            return books;
        });
    }
};
BookRepository = __decorate([
    typeorm_1.EntityRepository(book_entity_1.BookEntity)
], BookRepository);
exports.BookRepository = BookRepository;
//# sourceMappingURL=book.repository.js.map