import { Repository } from "typeorm";
import { BookReceiveModel } from "../models/books/book.receive.model";
import { BookModel } from "../models";
import { BookEntity } from "../entities/book.entity";
export declare class BookRepository extends Repository<BookEntity> {
    createBook(model: BookReceiveModel, dateTime: string): Promise<void>;
    getBookById(id: string): Promise<BookEntity>;
    updateBook(model: BookModel): Promise<void>;
    deleteBookById(id: string): Promise<void>;
    getAll(): Promise<BookEntity[]>;
}
