import { CategoryModel } from "../category.model";
export declare class MagazineReceiveModel {
    name: string;
    publisher: string;
    year: number;
    price: number;
    category: CategoryModel;
}
