import { MagazineEntity } from "../../entities/magazine.entity";
import { CategoryModel } from "../category.model";
import { ModelBase } from "../auth/base.model";
export declare class MagazineModel extends ModelBase {
    constructor(entity: MagazineEntity);
    name: string;
    publisher: string;
    year: number;
    price: number;
    category: CategoryModel;
}
