import { ProductModel } from "../poduct.model";
import { OrderEntity } from "../../entities/orders.entity";
export declare class OrderModel {
    constructor(model: OrderEntity);
    id: string;
    creationDate: string;
    buyerId: string;
    products: ProductModel;
}
