"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const poduct_model_1 = require("../poduct.model");
class OrderModel {
    constructor(model) {
        this.id = model.id.toString();
        this.creationDate = model.creationDate;
        this.buyerId = model.buyerId;
        this.products = model.products;
    }
}
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], OrderModel.prototype, "id", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], OrderModel.prototype, "creationDate", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], OrderModel.prototype, "buyerId", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", poduct_model_1.ProductModel)
], OrderModel.prototype, "products", void 0);
exports.OrderModel = OrderModel;
//# sourceMappingURL=order.model.js.map