import { BookModel } from "./books/book.model";
import { MagazineModel } from "./magazine/magazine.model";
export declare class ProductModel {
    constructor(booksModel: BookModel[], magazinesModel: MagazineModel[]);
    books: BookModel[];
    magazines: MagazineModel[];
}
