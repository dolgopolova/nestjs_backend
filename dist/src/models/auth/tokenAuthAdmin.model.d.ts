export declare class TokenAuthAdminModel {
    expiresIn: number | string;
    accessToken: string;
    firstName: string;
    lastName: string;
    email: string;
}
