export declare class TokenAuthUserModel {
    expiresIn: number | string;
    userId: string;
    accessToken: string;
    firstName: string;
    lastName: string;
    email: string;
}
