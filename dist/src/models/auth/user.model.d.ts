import { UserRole, UserEntity } from "../../entities/user.entity";
export declare class UserModel {
    constructor(entity: UserEntity);
    id: string;
    email: string;
    isEmailConfirmed: boolean;
    firstName: string;
    lastName: string;
    role: UserRole;
}
