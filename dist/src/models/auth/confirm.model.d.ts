export declare class ConfirmModel {
    email: string;
    expiresIn: number;
}
