export declare class ModelBase {
    constructor(id: string, creationDate: string);
    id: string;
    creationDate: string;
}
