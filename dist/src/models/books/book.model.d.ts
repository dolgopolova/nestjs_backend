import { BookEntity } from "../../entities/book.entity";
import { CategoryModel } from "../category.model";
import { ModelBase } from "../auth/base.model";
export declare class BookModel extends ModelBase {
    constructor(entity: BookEntity);
    name: string;
    authors: string;
    description: string;
    price: number;
    year: number;
    category: CategoryModel;
}
