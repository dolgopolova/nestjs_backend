import { CategoryModel } from "../category.model";
export declare class BookReceiveModel {
    name: string;
    authors: string;
    description: string;
    price: number;
    year: number;
    category: CategoryModel;
}
