"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./auth/tokenAuthUser.model"));
__export(require("./auth/tokenAuthAdmin.model"));
__export(require("./auth/signIn.model"));
__export(require("./auth/user.model"));
__export(require("./auth/signUp.model"));
__export(require("./books/book.model"));
__export(require("./magazine/magazine.model"));
//# sourceMappingURL=index.js.map