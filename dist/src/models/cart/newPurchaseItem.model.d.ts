import { ProductType } from "../../entities/purchaseItem.entity";
export declare class NewPurchaseItemModel {
    name: string;
    userId: string;
    productId: string;
    qty: number;
    type: ProductType;
    costsPerOne: number;
}
