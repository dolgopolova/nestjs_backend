import { PurchaseItemEntity, ProductType } from "../../entities/purchaseItem.entity";
export declare class PurchaseItemModel {
    constructor(purchaseEntity: PurchaseItemEntity);
    id: string;
    creationDate: string;
    name: string;
    userId: string;
    productId: string;
    qty: number;
    type: ProductType;
    costsPerOne: number;
}
