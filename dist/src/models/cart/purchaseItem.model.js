"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const purchaseItem_entity_1 = require("../../entities/purchaseItem.entity");
class PurchaseItemModel {
    constructor(purchaseEntity) {
        this.id = purchaseEntity.id.toString();
        this.creationDate = purchaseEntity.creationDate;
        this.name = purchaseEntity.name;
        this.userId = purchaseEntity.userId.toString();
        this.productId = purchaseEntity.productId.toString();
        this.qty = purchaseEntity.qty;
        this.type = purchaseEntity.type;
        this.costsPerOne = purchaseEntity.costsPerOne;
    }
}
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "id", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "creationDate", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "name", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "userId", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "productId", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", Number)
], PurchaseItemModel.prototype, "qty", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", String)
], PurchaseItemModel.prototype, "type", void 0);
__decorate([
    swagger_1.ApiModelProperty(),
    __metadata("design:type", Number)
], PurchaseItemModel.prototype, "costsPerOne", void 0);
exports.PurchaseItemModel = PurchaseItemModel;
//# sourceMappingURL=purchaseItem.model.js.map