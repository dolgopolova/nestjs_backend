"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./auth.service"));
__export(require("./product.service"));
__export(require("./book.service"));
__export(require("./magazine.service"));
__export(require("./order.service"));
//# sourceMappingURL=index.js.map