import { OrderModel } from "../models/orders/order.model";
import { OrderReceiveModel } from "../models/orders/order.receive.model";
export declare class OrderService {
    private readonly ordersRepository;
    constructor();
    createOrder(model: OrderReceiveModel): Promise<boolean>;
    getOrderById(id: string): Promise<OrderModel>;
    updateOrderById(model: OrderModel): Promise<boolean>;
    deleteOrderById(id: string): Promise<boolean>;
    getAllUserOrders(id: string): Promise<OrderModel[]>;
}
