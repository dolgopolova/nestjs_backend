import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";
export declare class PurchaseService {
    private cartRepository;
    constructor();
    createPurchase(userPurchase: NewPurchaseItemModel): Promise<boolean>;
    getPurchaseByUserId(id: string): Promise<PurchaseItemModel[]>;
    updatePurchaseByUserId(usersPurchase: PurchaseItemModel): Promise<boolean>;
    findPurchaseByUserIdProductId(userId: string, productId: string): Promise<PurchaseItemModel>;
    deletePurchaseByUserIdProductId(purchaseId: string): Promise<boolean>;
}
