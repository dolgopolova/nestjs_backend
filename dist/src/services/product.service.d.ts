import { BookRepository } from '../repositories/book.repository';
import { ProductModel } from '../models/poduct.model';
import { MagazineRepository } from '../repositories/magazine.repository';
export declare class ProductService {
    private readonly booksRepository;
    private readonly magazineRepository;
    constructor(booksRepository: BookRepository, magazineRepository: MagazineRepository);
    getAll(): Promise<ProductModel>;
}
