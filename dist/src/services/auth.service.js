"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const models_1 = require("../models");
const user_entity_1 = require("../entities/user.entity");
const md5_1 = require("ts-md5/dist/md5");
const user_repository_1 = require("../repositories/user.repository");
const typeorm_1 = require("typeorm");
const application_exception_1 = require("../common/exceptions/application.exception");
const jsonwebtoken_1 = require("jsonwebtoken");
const email_service_1 = require("./email.service");
let AuthService = class AuthService {
    constructor(jwtService, emailService) {
        this.jwtService = jwtService;
        this.emailService = emailService;
        this.userRepository = typeorm_1.getCustomRepository(user_repository_1.UserRepository, "TestDataBase");
    }
    signIn(model) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.findUser(model);
            if (!user.isEmailConfirmed) {
                throw new application_exception_1.ApplicationException(400, 'Please confirm your email to login');
            }
            let accessToken = null;
            if (user.role == user_entity_1.UserRole.CLIENT) {
                accessToken = this.jwtService.sign({
                    email: user.email,
                    role: user.role,
                    name: user.firstName,
                    surname: user.lastName
                }, { expiresIn: '1 day' });
            }
            return {
                expiresIn: '1 day',
                accessToken,
                userId: user.id.toString(),
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email
            };
        });
    }
    signInAdmin(model) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.findUser(model);
            let accessToken = null;
            if (user.role == user_entity_1.UserRole.ADMIN) {
                accessToken = this.jwtService.sign({
                    email: user.email,
                    role: user.role,
                    name: user.firstName,
                    surname: user.lastName
                }, { expiresIn: '1 day' });
            }
            return {
                expiresIn: '1 day',
                accessToken,
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email
            };
        });
    }
    signUp(model) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.userRepository.findUserByEmail(model.email);
            const passwordHash = md5_1.Md5.hashAsciiStr(model.password);
            let newUser = this.userRepository.create({
                email: model.email,
                isEmailConfirmed: false,
                firstName: model.firstName,
                lastName: model.lastName,
                passwordHash: passwordHash.toString(),
                role: user_entity_1.UserRole.CLIENT
            });
            this.userRepository.insert(newUser);
            this.emailService.sendMail(newUser.email);
            return {
                message: "You will receive an email with an account confirmation."
            };
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.userRepository.getAll();
            return users.map(user => {
                return new models_1.UserModel(user);
            });
        });
    }
    confirmEmail(emailToken) {
        return __awaiter(this, void 0, void 0, function* () {
            const confirm = jsonwebtoken_1.verify(emailToken, 'secretKey');
            if (!confirm) {
                throw new application_exception_1.ApplicationException(400, 'Verification timed out.');
            }
            const user = yield this.userRepository.findOne({ email: confirm.email });
            if (!user) {
                throw new application_exception_1.ApplicationException(400, 'User has not exist!');
            }
            user.isEmailConfirmed = true;
            yield this.userRepository.save(user);
            return {
                message: "Email is successfully confirmed"
            };
        });
    }
};
AuthService = __decorate([
    common_1.Injectable({
        scope: common_1.Scope.REQUEST
    }),
    __metadata("design:paramtypes", [jwt_1.JwtService, email_service_1.EmailService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map