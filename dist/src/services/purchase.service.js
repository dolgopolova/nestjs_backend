"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const purchaseItem_model_1 = require("../models/cart/purchaseItem.model");
const purchase_repository_1 = require("../repositories/purchase.repository");
let PurchaseService = class PurchaseService {
    constructor() {
        this.cartRepository = typeorm_1.getCustomRepository(purchase_repository_1.PurchaseRepository, "TestDataBase");
    }
    createPurchase(userPurchase) {
        return __awaiter(this, void 0, void 0, function* () {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;
            return yield this.cartRepository.createPurchase(userPurchase, dateTime);
        });
    }
    getPurchaseByUserId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const cart = yield this.cartRepository.getPurchaseByUserId(id);
            return cart.map(purchase => {
                return new purchaseItem_model_1.PurchaseItemModel(purchase);
            });
        });
    }
    updatePurchaseByUserId(usersPurchase) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartRepository.updatePurchaseByUserIdProductId(usersPurchase);
        });
    }
    findPurchaseByUserIdProductId(userId, productId) {
        return __awaiter(this, void 0, void 0, function* () {
            const purchase = yield this.cartRepository.findPurchaseByUserIdProductId(userId, productId);
            return new purchaseItem_model_1.PurchaseItemModel(purchase);
        });
    }
    deletePurchaseByUserIdProductId(purchaseId) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.cartRepository.deletePurchaseByUserIdProductId(purchaseId);
            return true;
        });
    }
};
PurchaseService = __decorate([
    common_1.Injectable({
        scope: common_1.Scope.REQUEST
    }),
    __metadata("design:paramtypes", [])
], PurchaseService);
exports.PurchaseService = PurchaseService;
//# sourceMappingURL=purchase.service.js.map