import { JwtService } from '@nestjs/jwt';
import { TokenAuthAdminModel, SignUpAuthModel, UserModel } from '../models';
import { SignInAuthModel } from '../models/auth/signIn.model';
import { ResultMessageModel } from '../models/auth/result.model';
import { EmailService } from './email.service';
import { TokenAuthUserModel } from '../models/auth/tokenAuthUser.model';
export declare class AuthService {
    private readonly jwtService;
    private readonly emailService;
    private userRepository;
    constructor(jwtService: JwtService, emailService: EmailService);
    signIn(model: SignInAuthModel): Promise<TokenAuthUserModel>;
    signInAdmin(model: SignInAuthModel): Promise<TokenAuthAdminModel>;
    signUp(model: SignUpAuthModel): Promise<ResultMessageModel>;
    getAll(): Promise<UserModel[]>;
    confirmEmail(emailToken: string): Promise<ResultMessageModel>;
}
