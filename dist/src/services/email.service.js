"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const application_exception_1 = require("../common/exceptions/application.exception");
const common_1 = require("../common");
const jsonwebtoken_1 = require("jsonwebtoken");
const common_2 = require("@nestjs/common");
let EmailService = class EmailService {
    sendMail(toEmail) {
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            auth: {
                type: "login",
                user: "nestmarketcreator@gmail.com",
                pass: "Qweasdzx123"
            }
        });
        const emailToken = jsonwebtoken_1.sign({
            email: toEmail
        }, 'secretKey', { expiresIn: '1d' });
        const url = `${common_1.environment.clientUrl}/account/confirmation/${emailToken}`;
        var mailOptions = {
            from: 'market@nestmarket.com',
            to: toEmail,
            subject: 'Account Verification',
            html: `Hello,\n\n Please verify your account by clicking the link: <a href='${url}'>${url}</a>`
        };
        transporter.sendMail(mailOptions, function (err) {
            if (err) {
                return new application_exception_1.ApplicationException(500, err.message);
            }
        });
    }
};
EmailService = __decorate([
    common_2.Injectable()
], EmailService);
exports.EmailService = EmailService;
//# sourceMappingURL=email.service.js.map