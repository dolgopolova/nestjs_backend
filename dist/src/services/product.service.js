"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const entities_1 = require("../entities");
const book_repository_1 = require("../repositories/book.repository");
const poduct_model_1 = require("../models/poduct.model");
const magazine_repository_1 = require("../repositories/magazine.repository");
const models_1 = require("../models");
let ProductService = class ProductService {
    constructor(booksRepository, magazineRepository) {
        this.booksRepository = booksRepository;
        this.magazineRepository = magazineRepository;
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const books = yield this.booksRepository.find();
            const magazines = yield this.magazineRepository.find();
            books.sort(function (a, b) {
                var textA = a.name.toLowerCase();
                var textB = b.name.toLowerCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            magazines.sort(function (a, b) {
                var textA = a.name.toLowerCase();
                var textB = b.name.toLowerCase();
                return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
            });
            const booksModel = books.map(book => { return new models_1.BookModel(book); });
            const magazineModel = magazines.map(magazine => { return new models_1.MagazineModel(magazine); });
            return new poduct_model_1.ProductModel(booksModel, magazineModel);
        });
    }
};
ProductService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(entities_1.BookEntity)),
    __param(1, typeorm_1.InjectRepository(entities_1.MagazineEntity)),
    __metadata("design:paramtypes", [book_repository_1.BookRepository,
        magazine_repository_1.MagazineRepository])
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=product.service.js.map