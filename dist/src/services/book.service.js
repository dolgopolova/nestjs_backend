"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const book_model_1 = require("../models/books/book.model");
const book_repository_1 = require("../repositories/book.repository");
const typeorm_1 = require("typeorm");
let BookService = class BookService {
    constructor() {
        this.bookRepository = typeorm_1.getCustomRepository(book_repository_1.BookRepository, "TestDataBase");
    }
    createBook(model) {
        return __awaiter(this, void 0, void 0, function* () {
            var today = new Date();
            var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date + ' ' + time;
            yield this.bookRepository.createBook(model, dateTime);
            return true;
        });
    }
    getBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const book = yield this.bookRepository.getBookById(id);
            return new book_model_1.BookModel(book);
        });
    }
    updateBook(model) {
        return __awaiter(this, void 0, void 0, function* () {
            const dateTime = this.getDate();
            model.creationDate = dateTime;
            yield this.bookRepository.updateBook(model);
            return true;
        });
    }
    deleteBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.bookRepository.deleteBookById(id);
            return true;
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const books = yield this.bookRepository.getAll();
            return books.map(book => {
                return new book_model_1.BookModel(book);
            });
        });
    }
    getDate() {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        return date + ' ' + time;
    }
};
BookService = __decorate([
    common_1.Injectable({
        scope: common_1.Scope.REQUEST
    }),
    __metadata("design:paramtypes", [])
], BookService);
exports.BookService = BookService;
//# sourceMappingURL=book.service.js.map