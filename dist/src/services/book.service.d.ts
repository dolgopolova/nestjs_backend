import { BookModel } from '../models/books/book.model';
import { BookReceiveModel } from '../models/books/book.receive.model';
export declare class BookService {
    private readonly bookRepository;
    constructor();
    createBook(model: BookReceiveModel): Promise<boolean>;
    getBookById(id: string): Promise<BookModel>;
    updateBook(model: BookModel): Promise<boolean>;
    deleteBookById(id: string): Promise<boolean>;
    getAll(): Promise<BookModel[]>;
    private getDate;
}
