import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
import { MagazineModel } from "../models";
export declare class MagazineService {
    private readonly magazineRepository;
    constructor();
    createMagazine(model: MagazineReceiveModel): Promise<boolean>;
    getMagazineById(id: string): Promise<MagazineModel>;
    updateMagazineById(model: MagazineModel): Promise<boolean>;
    deleteMagazineById(id: string): Promise<boolean>;
    getAll(): Promise<MagazineModel[]>;
    private getDate;
}
