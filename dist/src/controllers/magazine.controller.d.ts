import { MagazineService } from "../services";
import { MagazineModel } from "../models";
import { MagazineReceiveModel } from "../models/magazine/magazine.receive.model";
export declare class MagazineController {
    private magazineService;
    constructor(magazineService: MagazineService);
    createMagazine(model: MagazineReceiveModel): Promise<boolean>;
    getMagazineById(id: string): Promise<MagazineModel>;
    updateMagazineById(model: MagazineModel): Promise<boolean>;
    deleteMagazineById(id: string): Promise<boolean>;
    getAll(): Promise<MagazineModel[]>;
}
