"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const services_1 = require("../services");
const models_1 = require("../models");
const magazine_receive_model_1 = require("../models/magazine/magazine.receive.model");
const entities_1 = require("../entities");
const common_2 = require("../common");
const roles_guard_1 = require("../common/guards/roles.guard");
let MagazineController = class MagazineController {
    constructor(magazineService) {
        this.magazineService = magazineService;
    }
    createMagazine(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.magazineService.createMagazine(model);
        });
    }
    getMagazineById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.magazineService.getMagazineById(id);
        });
    }
    updateMagazineById(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.magazineService.updateMagazineById(model);
        });
    }
    deleteMagazineById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.magazineService.deleteMagazineById(id);
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.magazineService.getAll();
        });
    }
};
__decorate([
    common_1.Post('create'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Create magazine", operationId: "createMagazine" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [magazine_receive_model_1.MagazineReceiveModel]),
    __metadata("design:returntype", Promise)
], MagazineController.prototype, "createMagazine", null);
__decorate([
    common_1.Get('get/:id'),
    swagger_1.ApiOperation({ title: "Get magazine by its id", operationId: "getMagazine" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: models_1.MagazineModel }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MagazineController.prototype, "getMagazineById", null);
__decorate([
    common_1.Put('update'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Update magazine data", operationId: "updateMagazine" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [models_1.MagazineModel]),
    __metadata("design:returntype", Promise)
], MagazineController.prototype, "updateMagazineById", null);
__decorate([
    common_1.Delete('delete/:id'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Delete magazine by its id", operationId: "deleteMagazine" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], MagazineController.prototype, "deleteMagazineById", null);
__decorate([
    common_1.Get('getAll'),
    swagger_1.ApiOperation({ title: "Get all magazines in the system", operationId: "getAllMagazines" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: models_1.MagazineModel, isArray: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MagazineController.prototype, "getAll", null);
MagazineController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('Magazine'),
    common_1.Controller('api/magazine'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __metadata("design:paramtypes", [services_1.MagazineService])
], MagazineController);
exports.MagazineController = MagazineController;
//# sourceMappingURL=magazine.controller.js.map