"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const services_1 = require("../services");
const models_1 = require("../models");
const swagger_1 = require("@nestjs/swagger");
const book_receive_model_1 = require("../models/books/book.receive.model");
const entities_1 = require("../entities");
const common_2 = require("../common");
const roles_guard_1 = require("../common/guards/roles.guard");
let BookController = class BookController {
    constructor(booksService) {
        this.booksService = booksService;
    }
    createBook(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.booksService.createBook(model);
        });
    }
    getBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.booksService.getBookById(id);
        });
    }
    updateBook(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.booksService.updateBook(model);
        });
    }
    deleteBookById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.booksService.deleteBookById(id);
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.booksService.getAll();
        });
    }
};
__decorate([
    common_1.Post('create'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Create book", operationId: "createBook" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [book_receive_model_1.BookReceiveModel]),
    __metadata("design:returntype", Promise)
], BookController.prototype, "createBook", null);
__decorate([
    common_1.Get('get/:id'),
    swagger_1.ApiOperation({ title: "Get book by book id", operationId: "getBook" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: models_1.BookModel }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BookController.prototype, "getBookById", null);
__decorate([
    common_1.Put('update'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Update book data", operationId: "updateBook" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [models_1.BookModel]),
    __metadata("design:returntype", Promise)
], BookController.prototype, "updateBook", null);
__decorate([
    common_1.Delete('delete/:id'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Delete book by id", operationId: "deleteBook" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], BookController.prototype, "deleteBookById", null);
__decorate([
    common_1.Get('getAll'),
    swagger_1.ApiOperation({ title: "Get all books in the system", operationId: "getAllBooks" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: models_1.BookModel, isArray: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], BookController.prototype, "getAll", null);
BookController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('Book'),
    common_1.Controller('api/book'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __metadata("design:paramtypes", [services_1.BookService])
], BookController);
exports.BookController = BookController;
//# sourceMappingURL=book.controller.js.map