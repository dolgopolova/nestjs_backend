import { OrderReceiveModel } from '../models/orders/order.receive.model';
import { OrderService } from '../services/order.service';
import { OrderModel } from '../models/orders/order.model';
export declare class OrderController {
    private readonly basketService;
    constructor(basketService: OrderService);
    createOrder(model: OrderReceiveModel): Promise<boolean>;
    getOrderById(id: string): Promise<OrderModel>;
    updateOrderById(model: OrderModel): Promise<boolean>;
    deleteOrderById(id: string): Promise<boolean>;
    getAllUserOrders(id: string): Promise<OrderModel[]>;
}
