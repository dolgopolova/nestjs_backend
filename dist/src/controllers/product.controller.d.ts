import { ProductService } from '../services';
import { ProductModel } from '../models/poduct.model';
export declare class ProductController {
    private readonly productsService;
    constructor(productsService: ProductService);
    getAll(): Promise<ProductModel>;
}
