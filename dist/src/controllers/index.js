"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./auth.controller"));
__export(require("./product.controller"));
__export(require("./book.controller"));
__export(require("./magazine.controller"));
__export(require("./order.controller"));
//# sourceMappingURL=index.js.map