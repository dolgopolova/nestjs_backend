"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const services_1 = require("../services");
const poduct_model_1 = require("../models/poduct.model");
let ProductController = class ProductController {
    constructor(productsService) {
        this.productsService = productsService;
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.productsService.getAll();
        });
    }
};
__decorate([
    common_1.Get('getAll'),
    swagger_1.ApiOperation({ title: "Get all products in the system", operationId: "getAllProducts" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: poduct_model_1.ProductModel, isArray: true }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], ProductController.prototype, "getAll", null);
ProductController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('Products'),
    common_1.Controller('api/products'),
    __metadata("design:paramtypes", [services_1.ProductService])
], ProductController);
exports.ProductController = ProductController;
//# sourceMappingURL=product.controller.js.map