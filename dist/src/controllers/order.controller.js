"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const order_receive_model_1 = require("../models/orders/order.receive.model");
const order_service_1 = require("../services/order.service");
const order_model_1 = require("../models/orders/order.model");
const entities_1 = require("../entities");
const common_2 = require("../common");
const roles_guard_1 = require("../common/guards/roles.guard");
let OrderController = class OrderController {
    constructor(basketService) {
        this.basketService = basketService;
    }
    createOrder(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.basketService.createOrder(model);
        });
    }
    getOrderById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.basketService.getOrderById(id);
        });
    }
    updateOrderById(model) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.basketService.updateOrderById(model);
        });
    }
    deleteOrderById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.basketService.deleteOrderById(id);
        });
    }
    getAllUserOrders(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.basketService.getAllUserOrders(id);
        });
    }
};
__decorate([
    common_1.Post('create/order'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Create order", operationId: "createOrder" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [order_receive_model_1.OrderReceiveModel]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "createOrder", null);
__decorate([
    common_1.Get('get/orderById/:id'),
    common_2.Roles(entities_1.UserRole.CLIENT, entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Get order by its id", operationId: "getOrder" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: order_model_1.OrderModel }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "getOrderById", null);
__decorate([
    common_1.Put('update/order'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Update orders data", operationId: "updateOrder" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [order_model_1.OrderModel]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "updateOrderById", null);
__decorate([
    common_1.Delete('delete/order/:id'),
    common_2.Roles(entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Delete order by its id", operationId: "deleteOrder" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "deleteOrderById", null);
__decorate([
    common_1.Get('getAllUserOrders/:id'),
    common_2.Roles(entities_1.UserRole.CLIENT, entities_1.UserRole.ADMIN),
    swagger_1.ApiOperation({ title: "Get all user orders by user id", operationId: "getAllOrders" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: order_model_1.OrderModel, isArray: true }),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], OrderController.prototype, "getAllUserOrders", null);
OrderController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('Basket'),
    common_1.Controller('api/basket'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __metadata("design:paramtypes", [order_service_1.OrderService])
], OrderController);
exports.OrderController = OrderController;
//# sourceMappingURL=order.controller.js.map