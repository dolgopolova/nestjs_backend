import { BookService } from '../services';
import { BookModel } from '../models';
import { BookReceiveModel } from '../models/books/book.receive.model';
export declare class BookController {
    private booksService;
    constructor(booksService: BookService);
    createBook(model: BookReceiveModel): Promise<boolean>;
    getBookById(id: string): Promise<BookModel>;
    updateBook(model: BookModel): Promise<boolean>;
    deleteBookById(id: string): Promise<boolean>;
    getAll(): Promise<BookModel[]>;
}
