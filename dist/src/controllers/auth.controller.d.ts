import { AuthService } from '../services';
import { TokenAuthAdminModel, SignUpAuthModel, UserModel } from '../models';
import { SignInAuthModel } from '../models/auth/signIn.model';
import { ResultMessageModel } from '../models/auth/result.model';
import { TokenAuthUserModel } from '../models/auth/tokenAuthUser.model';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    signIn(model: SignInAuthModel): Promise<TokenAuthUserModel>;
    signInAdmin(model: SignInAuthModel): Promise<TokenAuthAdminModel>;
    signUp(model: SignUpAuthModel): Promise<ResultMessageModel>;
    getAll(): Promise<UserModel[]>;
    confirmEmail(emailToken: string): Promise<ResultMessageModel>;
}
