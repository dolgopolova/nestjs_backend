"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const roles_guard_1 = require("../common/guards/roles.guard");
const purchaseItem_model_1 = require("../models/cart/purchaseItem.model");
const purchase_service_1 = require("../services/purchase.service");
const common_2 = require("../common");
const entities_1 = require("../entities");
const newPurchaseItem_model_1 = require("../models/cart/newPurchaseItem.model");
let PurchaseController = class PurchaseController {
    constructor(cartService) {
        this.cartService = cartService;
    }
    createPurchase(userPurchase) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartService.createPurchase(userPurchase);
        });
    }
    getPurchaseByUserId(userId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartService.getPurchaseByUserId(userId);
        });
    }
    updateUsersPurchase(userPurchase) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartService.updatePurchaseByUserId(userPurchase);
        });
    }
    findPurchase(userId, productId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartService.findPurchaseByUserIdProductId(userId, productId);
        });
    }
    deletePurchase(purchaseId) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.cartService.deletePurchaseByUserIdProductId(purchaseId);
        });
    }
};
__decorate([
    common_1.Post('createPurchase'),
    common_2.Roles(entities_1.UserRole.CLIENT),
    swagger_1.ApiOperation({ title: "Creates a cart that will be tied to a specific user.", operationId: "createPurchase" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [newPurchaseItem_model_1.NewPurchaseItemModel]),
    __metadata("design:returntype", Promise)
], PurchaseController.prototype, "createPurchase", null);
__decorate([
    common_1.Get('getPurchaseByUserId/:userId'),
    common_2.Roles(entities_1.UserRole.CLIENT),
    swagger_1.ApiOperation({ title: "Returns items that are in the user’s basket.", operationId: "getPurchaseByUserId" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    swagger_1.ApiOkResponse({ type: purchaseItem_model_1.PurchaseItemModel, isArray: true }),
    __param(0, common_1.Param('userId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PurchaseController.prototype, "getPurchaseByUserId", null);
__decorate([
    common_1.Put('updatePurchase'),
    common_2.Roles(entities_1.UserRole.CLIENT),
    swagger_1.ApiOperation({ title: "Updates the cart on the id of the user to whom it belongs. If the cart is not in the database, it is created", operationId: "updatePurchaseByUserId" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [purchaseItem_model_1.PurchaseItemModel]),
    __metadata("design:returntype", Promise)
], PurchaseController.prototype, "updateUsersPurchase", null);
__decorate([
    common_1.Get('findPurchaseByUserIdProductId'),
    common_2.Roles(entities_1.UserRole.CLIENT),
    swagger_1.ApiOperation({ title: "Finds an existing purchase by user ID and product ID", operationId: "findPurchaseByUserIdProductId" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Query('userId')), __param(1, common_1.Query('productId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], PurchaseController.prototype, "findPurchase", null);
__decorate([
    common_1.Delete('delete/:purchaseId'),
    common_2.Roles(entities_1.UserRole.CLIENT),
    swagger_1.ApiOperation({ title: "Finds an existing purchase by user ID and product ID", operationId: "findPurchaseByUserIdProductId" }),
    swagger_1.ApiResponse({ status: 200, description: 'Success' }),
    swagger_1.ApiResponse({ status: 400, description: 'Bad Request' }),
    __param(0, common_1.Param('purchaseId')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PurchaseController.prototype, "deletePurchase", null);
PurchaseController = __decorate([
    swagger_1.ApiBearerAuth(),
    swagger_1.ApiUseTags('Purchase'),
    common_1.Controller('/api/purchase'),
    common_1.UseGuards(roles_guard_1.RolesGuard),
    __metadata("design:paramtypes", [purchase_service_1.PurchaseService])
], PurchaseController);
exports.PurchaseController = PurchaseController;
//# sourceMappingURL=purchase.controller.js.map