import { PurchaseItemModel } from "../models/cart/purchaseItem.model";
import { PurchaseService } from "../services/purchase.service";
import { NewPurchaseItemModel } from "../models/cart/newPurchaseItem.model";
export declare class PurchaseController {
    private readonly cartService;
    constructor(cartService: PurchaseService);
    createPurchase(userPurchase: NewPurchaseItemModel): Promise<boolean>;
    getPurchaseByUserId(userId: string): Promise<PurchaseItemModel[]>;
    updateUsersPurchase(userPurchase: PurchaseItemModel): Promise<boolean>;
    findPurchase(userId: string, productId: string): Promise<PurchaseItemModel>;
    deletePurchase(purchaseId: string): Promise<boolean>;
}
