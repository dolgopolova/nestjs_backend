"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const entities_1 = require("../entities");
const orders_entity_1 = require("../entities/orders.entity");
const purchaseItem_entity_1 = require("../entities/purchaseItem.entity");
const entities = [
    entities_1.EntityBase,
    entities_1.UserEntity,
    entities_1.BookEntity,
    entities_1.MagazineEntity,
    orders_entity_1.OrderEntity,
    purchaseItem_entity_1.PurchaseItemEntity
];
let DatabaseModule = class DatabaseModule {
};
DatabaseModule = __decorate([
    common_1.Global(),
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: "mongodb",
                name: "TestDataBase",
                url: "mongodb+srv://TestMongoDBUser:myNewMongoDBUser@testdatabase-mrz6w.mongodb.net/BookMarket?retryWrites=true",
                port: 3000,
                username: "TestMongoDBUser",
                password: "myNewMongoDBUser",
                database: "BookMarket",
                entities: [...entities],
                synchronize: true,
                useNewUrlParser: true
            }),
            typeorm_1.TypeOrmModule.forFeature([...entities], "TestDataBase"),
        ],
        exports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: "mongodb",
                name: "TestDataBase",
                url: "mongodb+srv://TestMongoDBUser:myNewMongoDBUser@testdatabase-mrz6w.mongodb.net/BookMarket?retryWrites=true",
                port: 3000,
                username: "TestMongoDBUser",
                password: "myNewMongoDBUser",
                database: "BookMarket",
                entities: [...entities],
                synchronize: true,
                useNewUrlParser: true
            }),
            typeorm_1.TypeOrmModule.forFeature([...entities], "TestDataBase"),
        ],
    })
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=database.module.js.map