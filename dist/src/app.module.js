"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const common_1 = require("@nestjs/common");
const book_module_1 = require("./modules/book.module");
const magazine_module_1 = require("./modules/magazine.module");
const product_module_1 = require("./modules/product.module");
const order_module_1 = require("./modules/order.module");
const auth_module_1 = require("./modules/auth.module");
const auth_middleware_1 = require("./common/middlewares/auth.middleware");
const controllers_1 = require("./controllers");
const purchase_controller_1 = require("./controllers/purchase.controller");
const purchase_module_1 = require("./modules/purchase.module");
const controllers = [
    controllers_1.AuthController,
    controllers_1.BookController,
    controllers_1.MagazineController,
    controllers_1.OrderController,
    controllers_1.ProductController,
    purchase_controller_1.PurchaseController
];
let ApplicationModule = class ApplicationModule {
    configure(consumer) {
        consumer
            .apply(auth_middleware_1.AuthMiddleware)
            .forRoutes(...controllers);
    }
};
ApplicationModule = __decorate([
    common_1.Module({
        imports: [
            auth_module_1.AuthModule, book_module_1.BookModule, magazine_module_1.MagazineModule, order_module_1.OrderModule, product_module_1.ProductModule, purchase_module_1.PurchaseModule
        ],
    })
], ApplicationModule);
exports.ApplicationModule = ApplicationModule;
//# sourceMappingURL=app.module.js.map