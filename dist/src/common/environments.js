"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Environments;
(function (Environments) {
    Environments[Environments["Development"] = 0] = "Development";
    Environments[Environments["Production"] = 1] = "Production";
})(Environments = exports.Environments || (exports.Environments = {}));
exports.environment = {
    production: false,
    clientUrl: 'http://localhost:8100'
};
//# sourceMappingURL=environments.js.map