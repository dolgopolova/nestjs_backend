export declare enum Environments {
    'Development' = 0,
    'Production' = 1
}
export declare const environment: {
    production: boolean;
    clientUrl: string;
};
