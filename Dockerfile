FROM node:10.13-alpine as buildContainer

WORKDIR /app
COPY ./package.json ./package-lock.json ./tsconfig.json /app/
##RUN npm install -g ts-node nodemon
RUN npm install

WORKDIR /app
##RUN npm install typescript
COPY . /app
RUN npm run build

FROM node:8-alpine

WORKDIR /app

# Copy dependency definitions
COPY --from=buildContainer /app/package.json /app
COPY --from=buildContainer /app/package-lock.json /app
COPY --from=buildContainer /app/tsconfig.json /app

# Get all the code needed to run the app
COPY --from=buildContainer /app/dist /app/dist
COPY --from=buildContainer /app/node_modules /app/node_modules
# Expose the port the app runs in
EXPOSE 3000

CMD ["npm", "run", "start"]